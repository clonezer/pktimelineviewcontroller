//
//  Reservation.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import Foundation

struct Reservation {
    var unitId: Int
    var guestName: String
    var startAt: Int
    var endAt: Int
}

extension Reservation: Codable {
    enum CodingKeys: String, CodingKey {
        case unitId = "unit id"
        case guestName = "guest name"
        case startAt = "start at"
        case endAt = "end at"
    }
}
