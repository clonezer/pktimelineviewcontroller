//
//  UnitTypeData.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import Foundation

struct UnitTypeData {
    var unitType: String
    var unitTypeName: String
    var units: [String]
    var reservations: [Reservation]
}

extension UnitTypeData: Codable {
    enum CodingKeys: String, CodingKey {
       case unitType = "unit type"
       case unitTypeName = "unit type name"
       case units
       case reservations
    }
}
