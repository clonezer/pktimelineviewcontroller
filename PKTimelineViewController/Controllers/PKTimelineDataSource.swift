//
//  PKTimelineDataSource.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

typealias CellConfigurationBlock = (_ cell: ScheduleCell, _ indexPath: NSIndexPath, _ reservation: Reservation) -> ()
typealias RowHeaderConfigurationBlock = (_ header: ScheduleRowHeader, _ indexPath: NSIndexPath) -> ()
typealias ColumnHeaderConfigurationBlock = (_ header: ScheduleColumnHeader, _ indexPath: NSIndexPath) -> ()
typealias BackgroundConfigurationBlock = (_ background: ScheduleBackground, _ indexPath: NSIndexPath, _ kind: String) -> ()

class PKTimelineDataSource: NSObject, UICollectionViewDataSource {
    
    var numberOfRowsInSchedule: Int!
    var numberOfColumnsInSchedule: Int!
    var rowHeaderTitles: [String]!
    var cellConfigurationBlock: CellConfigurationBlock?
    var rowHeaderConfigurationBlock: RowHeaderConfigurationBlock?
    var columnHeaderConfigurationBlock: ColumnHeaderConfigurationBlock?
    var backgroundConfigurationBlock: BackgroundConfigurationBlock?
    
    lazy var schedule: [UnitTypeData] = {
        if let path = Bundle.main.path(forResource: "reservations", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                guard let unitTypeDatas = try? JSONDecoder().decode([UnitTypeData].self, from: data)
                else {
                    return []
                }
                return unitTypeDatas
              }
            catch {
                return []
            }
        }
        return []
    }()
    
    func setDefaults() {
        rowHeaderTitles = {
            var names: [String] = []
            let groups = schedule
            for group in groups {
                names += group.units
            }
            return names
        }()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return schedule.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let group = schedule[section]
        return group.reservations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScheduleCell", for: indexPath) as! ScheduleCell
        let group = schedule[indexPath.section]
        let reservation = group.reservations[indexPath.item]
        if let configurationBlock = cellConfigurationBlock {
            configurationBlock(cell, indexPath as NSIndexPath, reservation)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case "RowHeader":
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ScheduleRowHeader", for: indexPath) as! ScheduleRowHeader
            if let config = rowHeaderConfigurationBlock {
                config(header, indexPath as NSIndexPath)
            }
            return header
        case "ColumnHeader":
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ScheduleColumnHeader", for: indexPath) as! ScheduleColumnHeader
            if let config = columnHeaderConfigurationBlock {
                config(header, indexPath as NSIndexPath)
            }
            return header
        case "EdgeCell":
            let edgeCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ScheduleEdgeCell", for: indexPath) as! ScheduleEdgeCell
            return edgeCell
        default:
            let background = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ScheduleBackground", for: indexPath) as! ScheduleBackground
            if let config = backgroundConfigurationBlock {
                config(background, indexPath as NSIndexPath, kind)
            }
            return background
        }
    }
    
    internal func indexPathOfColumnHeaderViews() -> NSArray {
        let indexPaths = NSMutableArray()
        for item in 0..<numberOfColumnsInSchedule {
            let indexPath = NSIndexPath(item: item, section: 0)
            indexPaths.adding(indexPath)
        }
        return indexPaths
    }
    
    internal func indexPathOfRowHeaderViews() -> NSArray {
        let indexPaths = NSMutableArray()
        for section in 0..<schedule.count {
            let group = schedule[section]
            for item in 0..<group.units.count {
                let indexPath = NSIndexPath(item: item, section: 0)
                indexPaths.adding(indexPath)
            }
        }
        return indexPaths
    }
    
    func reservationForIndexPath(indexPath: NSIndexPath) -> Reservation {
        let group = schedule[indexPath.section]
        return group.reservations[indexPath.item]
    }
    
    func titleForColumnHeaderViewAtIndexPath(indexPath: NSIndexPath) -> String {
        return "\(indexPath.item + 1)"
    }
    
    func titleForRowHeaderViewAtIndexPath(indexPath: NSIndexPath) -> String {
        return "\(rowHeaderTitles[indexPath.item])"
    }
}
