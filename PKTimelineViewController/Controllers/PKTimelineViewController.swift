//
//  PKTimelineViewController.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

private let SCHEDULE_ROW_HEADER = "ScheduleRowHeader"
private let SCHEDULE_COLUMN_HEADER = "ScheduleColumnHeader"
private let SCHEDULE_BACKGROUND = "ScheduleBackground"
private let SCHEDULE_EDGE = "ScheduleEdgeCell"

private let ROW_HEADER = "RowHeader"
private let COLUMN_HEADER = "ColumnHeader"
private let EDGE_HEADER = "EdgeCell"
private let ROW_BACKGROUND = "RowBackground"
private let COLUMN_BACKGROUND = "ColumnBackground"

class PKTimelineViewController: UICollectionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefaultsCollectionView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDefaultsCollectionView() {
        // Register cell classes
        let rowHeaderNib = UINib(nibName: SCHEDULE_ROW_HEADER, bundle: nil)
        let columnCeaderNib = UINib(nibName: SCHEDULE_COLUMN_HEADER, bundle: nil)
        let backgroundNib = UINib(nibName: SCHEDULE_BACKGROUND, bundle: nil)
        let edgeCellNib = UINib(nibName: SCHEDULE_EDGE, bundle: nil)
        
        collectionView!.register(rowHeaderNib, forSupplementaryViewOfKind: ROW_HEADER, withReuseIdentifier: SCHEDULE_ROW_HEADER)
        collectionView!.register(columnCeaderNib, forSupplementaryViewOfKind: COLUMN_HEADER, withReuseIdentifier: SCHEDULE_COLUMN_HEADER)
        collectionView!.register(backgroundNib, forSupplementaryViewOfKind: COLUMN_BACKGROUND, withReuseIdentifier: SCHEDULE_BACKGROUND)
        collectionView!.register(backgroundNib, forSupplementaryViewOfKind: ROW_BACKGROUND, withReuseIdentifier: SCHEDULE_BACKGROUND)
        collectionView!.register(edgeCellNib, forSupplementaryViewOfKind: EDGE_HEADER, withReuseIdentifier: SCHEDULE_EDGE)
        
        // Do any additional setup after loading the view.
        let dataSource = collectionView!.dataSource as! PKTimelineDataSource
        dataSource.setDefaults()
        
        let schedule = dataSource.schedule
        
        var numberOfRow: Int = 0
        
        for group in schedule {
            numberOfRow = numberOfRow + group.units.count
        }
        
        dataSource.numberOfRowsInSchedule = numberOfRow
        dataSource.numberOfColumnsInSchedule = 31
        
        dataSource.cellConfigurationBlock = {(cell: ScheduleCell, indexPath: NSIndexPath, reservation: Reservation) in
            let days = reservation.endAt - reservation.startAt
            cell.layer.shouldRasterize = true
            cell.layer.rasterizationScale = UIScreen.main.scale
            cell.nameLabel.text = reservation.guestName
            cell.timeLabel.text = "(\(days) \(days > 1 ? "days" : "day"))"
            cell.layer.shouldRasterize = true
            cell.layer.rasterizationScale = UIScreen.main.scale
        }
        
        dataSource.rowHeaderConfigurationBlock = {(header: ScheduleRowHeader, indexPath: NSIndexPath) in
            header.layer.shouldRasterize = true
            header.layer.rasterizationScale = UIScreen.main.scale
            header.rowTitleLabel.text = dataSource.titleForRowHeaderViewAtIndexPath(indexPath: indexPath)
        }
        
        dataSource.columnHeaderConfigurationBlock = {(header: ScheduleColumnHeader, indexPath: NSIndexPath) in
            header.layer.shouldRasterize = true
            header.layer.rasterizationScale = UIScreen.main.scale
            header.titleLabel.text = dataSource.titleForColumnHeaderViewAtIndexPath(indexPath: indexPath)
            header.leftVerticalLineView.isHidden = false
            header.rightVerticalLineView.isHidden = true
            header.bottomLineView.isHidden = false
        }
        
        dataSource.backgroundConfigurationBlock = {(background: ScheduleBackground, indexPath: NSIndexPath, kind: String) in
            if kind == COLUMN_BACKGROUND {
                background.verticalLineView.isHidden = false
                background.horizontalLineView.isHidden = true
            }else {
                background.verticalLineView.isHidden = true
                background.horizontalLineView.isHidden = false
            }
        }
        
        let delegate = collectionView!.delegate as! PKTimelineDelegate
        delegate.cellDidSelectItemBlock = {(indexPath: NSIndexPath, reservation: Reservation) in
            
            let alertController = UIAlertController(title: "Hola!", message: "select: \(reservation.guestName)", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                print("Show details")
            }
            alertController.addAction(OKAction)
                self.present(alertController, animated: true) {
            }
        }
    }
    
}
