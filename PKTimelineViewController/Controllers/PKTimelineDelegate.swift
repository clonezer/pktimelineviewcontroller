//
//  PKTimelineDelegate.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

typealias CellDidSelectItemBlock = (_ indexPath: NSIndexPath, _ reservation: Reservation) -> ()

class PKTimelineDelegate: NSObject, UICollectionViewDelegate {
    var cellDidSelectItemBlock: CellDidSelectItemBlock?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let didSelectItemBlock = cellDidSelectItemBlock {
            let datasource = collectionView.dataSource as! PKTimelineDataSource
            let group = datasource.schedule[indexPath.section]
            didSelectItemBlock(indexPath as NSIndexPath, group.reservations[indexPath.row])
        }
    }
}
