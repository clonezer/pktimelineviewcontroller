//
//  ScheduleColumnHeader.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

class ScheduleColumnHeader: UICollectionReusableView {

@IBOutlet weak var titleLabel: UILabel!

@IBOutlet weak var leftVerticalLineView: UIView!
@IBOutlet weak var rightVerticalLineView: UIView!
@IBOutlet weak var bottomLineView: UIView!
override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
}
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return layoutAttributes
    }
    
}
