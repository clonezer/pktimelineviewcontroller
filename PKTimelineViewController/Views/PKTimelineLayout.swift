//
//  PKTimelineLayout.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

class PKTimelineLayout: UICollectionViewLayout {
    
    private lazy var dataSource: PKTimelineDataSource = {
        return self.collectionView!.dataSource as! PKTimelineDataSource
    }()
    
    private let columnWidth: CGFloat = 120
    private let columnHeaderHeight: CGFloat = 70
    private let rowHeaderWidth: CGFloat = 120
    private let rowHeight: CGFloat = 60
    private let cellVerticalSpacing: CGFloat = 1
    private let cellHorizontalSpacing: CGFloat = 2
    private let sectionVerticalSpacing: CGFloat = 0
    
    struct Frame {
        var rect: CGRect
        var indexPath: NSIndexPath
    }
    
    var reservationFrames: [Frame] = []
    var colunmBackgroundFrames: [Frame] = []
    var rowBackgroundFrames: [Frame] = []
    var columnHeaderFrames: [Frame] = []
    var rowHeaderFrames: [Frame] = []
    var edgeCellFrames: [Frame] = []
    
    private func frameForReservation(reservation: Reservation, atIndexPath indexPath: NSIndexPath) -> CGRect {
        
        var yOffset: CGFloat = 0
        
        if indexPath.section > 0 {
            for section in 0..<indexPath.section {
                yOffset += CGFloat(dataSource.schedule[section].units.count) * rowHeight
            }
        }
        
        let width = (columnWidth * CGFloat(reservation.endAt - reservation.startAt)) + (columnWidth/4) - cellHorizontalSpacing
        let x = rowHeaderWidth + ((CGFloat(reservation.startAt) - 1) * columnWidth) + (columnWidth/4)
        let y = yOffset + columnHeaderHeight + (rowHeight * CGFloat(reservation.unitId)) + (CGFloat(indexPath.section) * sectionVerticalSpacing)
        let frame = CGRect(x: x, y: y + cellVerticalSpacing, width: width, height: rowHeight - (cellVerticalSpacing * 2))
        
        return frame
    }
    
    private func frameForColoumnBackgroundViewAtIndexPath(indexPath: NSIndexPath) -> CGRect {
        let frame = CGRect(
                x: rowHeaderWidth + (columnWidth * CGFloat(indexPath.item)),
                y: collectionView!.contentOffset.y,
                width: columnWidth,
                height: collectionViewContentSize.height
        )
        return frame
    }
       
    private func frameForRowBackgroundViewAtIndexPath(indexPath: NSIndexPath) -> CGRect {
        let frame = CGRect(
                x: collectionView!.contentOffset.x,
                y: columnHeaderHeight + (rowHeight * CGFloat(indexPath.item)),
                width:collectionViewContentSize.width,
                height: rowHeight
            )
        return frame
    }
    
    private func frameForColumnHeaderViewAtIndexPath(indexPath: NSIndexPath) -> CGRect {
        
        let x: CGFloat = columnWidth + (columnWidth * CGFloat(indexPath.item))
        let y: CGFloat = collectionView!.contentOffset.y
        let width: CGFloat = columnWidth
        let height: CGFloat = columnHeaderHeight
        
        let frame = CGRect(x: x, y: y, width: width, height: height)
        
        return frame
    }
    
    private func frameForRowHeaderViewAtIndexPath(indexPath: NSIndexPath) -> CGRect {
        let frame = CGRect(
            x: collectionView!.contentOffset.x,
            y: columnHeaderHeight + (rowHeight * CGFloat(indexPath.item)),
            width:rowHeaderWidth,
            height: rowHeight
        )
        return frame
    }
    
    private func frameForEdgeCell() -> CGRect {
        let frame = CGRect(
            x: collectionView!.contentOffset.x,
            y: collectionView!.contentOffset.y,
            width: rowHeaderWidth,
            height: columnHeaderHeight
        )
        return frame
    }
    
    override var collectionViewContentSize: CGSize {
        let columnsInSchedule = CGFloat(dataSource.numberOfColumnsInSchedule)
        let height = (CGFloat(dataSource.numberOfRowsInSchedule) * rowHeight) + columnHeaderHeight
        let width = rowHeaderWidth + (columnsInSchedule * columnWidth)
        
        return CGSize(width: width, height: height)
    }
    
    override func prepare() {
        super.prepare()
        
        if edgeCellFrames.isEmpty {
            let indexPath = NSIndexPath(item: 0, section: 0)
            let rect = frameForEdgeCell()
            let frame = Frame(rect: rect, indexPath: indexPath)
            edgeCellFrames.append(frame)
        }
        
        if reservationFrames.isEmpty {
            for section in 0..<collectionView!.numberOfSections {
                for item in 0..<collectionView!.numberOfItems(inSection: section) {
                    let indexPath = NSIndexPath(item: item, section: section)
                    let rect = frameForReservation(
                        reservation: dataSource.reservationForIndexPath(indexPath: indexPath),
                        atIndexPath: indexPath)
                    let frame = Frame(rect: rect, indexPath: indexPath)
                    reservationFrames.append(frame)
                }
            }
        }
        
        if colunmBackgroundFrames.isEmpty {
            for column in 0..<dataSource.numberOfColumnsInSchedule {
                let indexPath = NSIndexPath(item: column, section: 0)
                let rect = frameForColoumnBackgroundViewAtIndexPath(indexPath: indexPath)
                let frame = Frame(rect: rect, indexPath: indexPath)
                colunmBackgroundFrames.append(frame)
            }
        }
        
        if rowBackgroundFrames.isEmpty {
            for column in 0..<dataSource.numberOfColumnsInSchedule {
                let indexPath = NSIndexPath(item: column, section: 0)
                let rect = frameForRowBackgroundViewAtIndexPath(indexPath: indexPath)
                let frame = Frame(rect: rect, indexPath: indexPath)
                rowBackgroundFrames.append(frame)
            }
        }
        
        if columnHeaderFrames.isEmpty {
            for column in 0..<dataSource.numberOfColumnsInSchedule {
                let indexPath = NSIndexPath(item: column, section: 0)
                let rect = frameForColumnHeaderViewAtIndexPath(indexPath: indexPath)
                let frame = Frame(rect: rect, indexPath: indexPath)
                columnHeaderFrames.append(frame)
            }
        }
        
        if rowHeaderFrames.isEmpty {
            for row in 0..<dataSource.numberOfRowsInSchedule {
                let indexPath = NSIndexPath(item: row, section: 0)
                let rect = frameForRowHeaderViewAtIndexPath(indexPath: indexPath)
                let frame = Frame(rect: rect, indexPath: indexPath)
                rowHeaderFrames.append(frame)
            }
        }
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes: [UICollectionViewLayoutAttributes] = []
        

        for frame in reservationFrames {
            if frame.rect.intersects(rect) {
                let attributes = UICollectionViewLayoutAttributes(forCellWith: frame.indexPath as IndexPath)
                attributes.frame = frame.rect
                layoutAttributes.append(attributes)
            }
        }
        
        for frame in colunmBackgroundFrames {
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: "ColumnBackground", with: frame.indexPath as IndexPath)
            let yOffset = collectionView!.contentOffset.y
            attributes.frame = CGRect(x: frame.rect.origin.x, y: yOffset, width: frame.rect.width, height: frame.rect.height)
            attributes.zIndex = -1
            layoutAttributes.append(attributes)
        }
        
        for frame in rowBackgroundFrames {
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: "RowBackground", with: frame.indexPath as IndexPath)
            let xOffset = collectionView!.contentOffset.x
            attributes.frame = CGRect(x: xOffset, y: frame.rect.origin.y, width: frame.rect.width, height: frame.rect.height)
            attributes.zIndex = -2
            layoutAttributes.append(attributes)
        }
        
        for frame in edgeCellFrames {
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: "EdgeCell", with: frame.indexPath as IndexPath)
            let xOffset = collectionView!.contentOffset.x
            let yOffset = collectionView!.contentOffset.y
            attributes.frame = CGRect(x: xOffset, y: yOffset, width: frame.rect.width, height: frame.rect.height)
            attributes.zIndex = 1025
            layoutAttributes.append(attributes)
        }
        
        for frame in columnHeaderFrames {
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: "ColumnHeader", with: frame.indexPath as IndexPath)
            let yOffset = collectionView!.contentOffset.y
            
            attributes.frame = CGRect(x: frame.rect.origin.x, y: yOffset, width: frame.rect.width, height: frame.rect.height)
            attributes.zIndex = 1024
            layoutAttributes.append(attributes)
        }
        
        for frame in rowHeaderFrames {
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: "RowHeader", with: frame.indexPath as IndexPath)
            let xOffset = collectionView!.contentOffset.x
            attributes.frame = CGRect(x: xOffset, y: frame.rect.origin.y, width: frame.rect.width, height: frame.rect.height)
            attributes.zIndex = 1023
            layoutAttributes.append(attributes)
        }
        
        return layoutAttributes
    }
}
