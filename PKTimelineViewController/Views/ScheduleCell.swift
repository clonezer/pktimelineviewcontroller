//
//  ScheduleCell.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

class ScheduleCell: UICollectionViewCell  {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var itemBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let mask = CAShapeLayer()
        let path = CGMutablePath()
        
        let width = self.layer.bounds.size.width
        let height = self.layer.bounds.size.height
        let offSet: CGFloat = 30
        
        mask.frame = itemBackgroundView!.layer.bounds
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: width - offSet, y: 0))
        path.addLine(to: CGPoint(x: width, y: height))
        path.addLine(to: CGPoint(x: offSet, y: height))
        path.addLine(to: CGPoint(x: 0, y: 0))
        
        mask.path = path
        itemBackgroundView!.layer.mask = mask
        
        let shape = CAShapeLayer()
        shape.frame = self.bounds
        shape.path = path
        shape.fillColor = UIColor.clear.cgColor
        
        itemBackgroundView!.layer.insertSublayer(shape, at: 0)
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return layoutAttributes
    }
}
