//
//  ScheduleEdgeCell.swift
//  PKTimelineViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

class ScheduleEdgeCell: UICollectionReusableView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return layoutAttributes
    }
}
